#pragma once

#include <limits>
#include <memory>
#include <set>
#include <stdexcept>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <atomic>

#include <folly/SocketAddress.h>
#include <folly/futures/Future.h>
#include <folly/io/async/AsyncServerSocket.h>
#include <folly/io/async/AsyncSocket.h>
#include <folly/io/async/AsyncSocketException.h>
#include <folly/io/async/AsyncTransport.h>

#include "src/main/cpp/io_callbacks.h"

namespace xmpl {

namespace socket {

/**
 * AsyncServerSocket requires one event_base thread for listening and at least one
 * event_base thread for handling connections. Each handling event_base will have
 * exactly one AcceptCallback attached to it. New connections coming in will trigger
 * corresponding AcceptCallback of that event_base.
 */
class SocketServer : protected folly::AsyncServerSocket {
 protected:
  using ReadCallback = xmpl::socket::ReadCallback;
  using WriteCallback = xmpl::socket::WriteCallback;

  // Will be trigger on the handling event_base
  class AcceptCallback : public folly::AsyncServerSocket::AcceptCallback {
   public:
    struct ConnectionHandler {
      ConnectionHandler(std::shared_ptr<ReadCallback> rcb,
                        std::shared_ptr<WriteCallback> wcb,
                        std::shared_ptr<folly::AsyncSocket> as)
          : read_callback(rcb),
            write_callback(wcb),
            async_socket(as),
            eof_reached(false),
            pending_write(false),
            id(++current_id) {
      }

      std::shared_ptr<ReadCallback> read_callback;
      std::shared_ptr<WriteCallback> write_callback;
      std::shared_ptr<folly::AsyncSocket> async_socket;
      bool eof_reached;
      bool pending_write;
      size_t id;

      // This is global and shared by all threads, so need to synchronize
      static std::atomic_size_t current_id;
    };

    AcceptCallback(SocketServer &sv, folly::EventBase *event_base)
        : server_(&sv), event_base_(event_base) {
    }

    void connectionAccepted(folly::NetworkSocket fd,
                            const folly::SocketAddress& client_addr) noexcept override;

    void acceptError(const std::exception& ex) noexcept override {
      server_->HandleError(ex);
    }


    void RemoveConnection(std::list<ConnectionHandler>::iterator it) {
      it->async_socket->close();
      conn_handlers_.erase(it);
    }

   private:
    SocketServer *server_ = nullptr;
    folly::EventBase *event_base_ = nullptr;
    // This list will always be used in the same AcceptCallback thread, so no need to synchronize
    // it. We use std::list so that we can freely remove a connection handler without
    // affective/invalidating other connection handlers.
    std::list<ConnectionHandler> conn_handlers_;
  };

  virtual folly::Future<std::string> HandleRequest(folly::EventBase *event_base,
                                                   const folly::SocketAddress &client_addr,
                                                   const char *buffer,
                                                   size_t len) noexcept {
    return HandleRequest(client_addr, buffer, len);
  }

  virtual folly::Future<std::string> HandleRequest(const folly::SocketAddress &client_addr,
                                                   const char *buffer,
                                                   size_t len) noexcept {
    return HandleRequest(buffer, len);
  }

  virtual folly::Future<std::string> HandleRequest(const char *buffer, size_t len) noexcept {
    LOG(ERROR) << "Request handler not implemented";
    return folly::Future<std::string>("");
  }

  virtual void HandleError(const std::exception& ex) noexcept = 0;

 public:
  static constexpr int DEFAULT_BACKLOG = 1;

  SocketServer() { }
  void InitializeEventBase(folly::EventBase *main_evbs, const std::vector<folly::EventBase*> evbs);

  virtual void Start(int port, int backlog = DEFAULT_BACKLOG);

  folly::EventBase *main_event_base() {
    return main_event_base_;
  }

  void Destroy();

 protected:
  std::vector<std::shared_ptr<folly::AsyncServerSocket::AcceptCallback>> callbacks_;
  folly::EventBase *main_event_base_ = nullptr;
};

} // namespace socket

} // namespace xmpl
